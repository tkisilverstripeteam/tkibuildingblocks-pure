# Building Blocks Templates

This module provides templates for the Building Blocks module, with a dependency on Pure CSS.

## Installation
The module may be installed with composer:

    composer require "tkisilverstripeteam/tkibuildingblocks-pure:*"

