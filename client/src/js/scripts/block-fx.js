/*!
 * Block Effects 0.1.0
 * 
 * Copyright (c) 2019 Todd Hossack todd(at)tiraki.com
 * http://www.tiraki.com
 * 
 * Depends:
 * jquery
 */
require('jquery');

(function($, undefined) {
	$(document).ready(function(){
		/* Accordion */
		$('.bb-fx__accordion').each(function(index,el) {
			var $content = $(this);
			var $hdr = $(this).find('.bb-fx__accordion-title').eq(0);
				// Proceed only if we have a header
			if($hdr.length) {
				$hdr.addClass('bb-fx-js')
					.insertBefore($(this))
					.css({'cursor':'pointer','margin-top' : 0,
						'margin-right' : 0,'margin-left' : 0 })
					.click(function() {
						$content.toggle();
						$(this).toggleClass('bb-fx__accordion--inactive');
						return false;
					});
				if(!$content.hasClass('bb-fx-js')) {
					$content.addClass('bb-fx-js');
					if(!$content.hasClass('bb-fx__accordion--active')) {
						$content.hide();
						$content.toggleClass('bb-fx__accordion--inactive',true);
						$hdr.toggleClass('bb-fx__accordion--inactive',true);
					}
				}
			}
		});
		/* Dropdown list */
		$('.bb-fx__dropdown').each(function(index,el) {
			var $container = $(this);
			$(this).find('.dropdown-list').each(function(index2,el2) {
				var $list = $(this);
				$list.addClass('bb-fx-js');
				var $select = $('<p class="bb-fx__dropdown-select bb-fx-js">Select one</p>');
				$select.insertBefore($list).click(function() {
					$list.toggle();
					$(this).toggleClass('bb-fx__dropdown--inactive');
					return false;
				});
				if(!$container.hasClass('bb-fx__dropdown--active')) {
					$list.hide();
					$list.toggleClass('bb-fx__dropdown--inactive',true);
					$select.toggleClass('bb-fx__dropdown--inactive',true);
				}
			});

		});
	});
})(jQuery);
