/*
 * Vendor
 */

// PureCSS components
require('purecss/build/grids.css');
require('purecss/build/grids-responsive.css');
require('purecss/build/menus.css');
require('../styles/lib/css/pure-custom.css');

// Fonts/Icons
require('font-awesome/scss/font-awesome.scss');

// Slideshow components
require('animate.css/animate.css');
require('owl.carousel/src/scss/owl.carousel.scss');
require('owl.carousel/src/scss/owl.theme.default.scss');

require('imports-loader?jQuery=jquery!owl.carousel');
