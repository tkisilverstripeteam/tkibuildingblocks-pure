<div class="bb-layout bb-layout-links $ViewClass">
	<% if $ViewItems.count %>
	<div class="bb-link__items">
		<% loop $ViewItems %>
			<div class="bb-link__item">
				<% if $MediaElement %>
				<div class="bb-link__itemimage">
					<a href="$Link.ATT" target="$Target">$MediaElement</a>
				</div>
				<% end_if %>
				<div class="bb-link__itembody">
					<h5 class="bb-link__itemtitle"><a class="bb-link__itemtitle" href="$Link.ATT" target="$Target">$Title</a></h5>
					<% if $Description %><p class="bb-link__itemdesc">$Description</p><% end_if %>
				</div>
			</div>
		<% end_loop %>
	</div>
	<% end_if %>
</div>